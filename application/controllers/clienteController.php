<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ClienteController extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('array');
		$this->load->library('table');
		$this->load->library('form_validation');
		$this->load->model('clientemodel');
	}

	public function index(){
		$this->load->view('index');

		if($this->input->post()){
			$parametros = $this->input->post();
			$erro = FALSE;

			if($parametros){
				$this->form_validation->set_rules('nome', 'Nome', 'trim');
				$this->form_validation->set_rules('estado','Estado','trim');
				$this->form_validation->set_rules('data','Data','trim');

				if($this->form_validation->run()==FALSE){
					$erro = TRUE;
				}
				
				if(!$erro){
					$dados = array(
						'nome' => $parametros['nome'],
						'estado' => $parametros['select'],
						'data' => $parametros['data']);

					$this->clientemodel->inserir($dados);
				}
			}
		}
		
	}

	public function buscar(){
		$resultadoBusca = $this->clientemodel->busca();
		$dados['resultado_busca'] = $resultadoBusca;
		
		$this->load->view('buscar',$dados);
	}

	public function editar(){
		$id = $this->uri->segment(3);
		if($this->input->post()){
			$parametros = $this->input->post();
			

			$dados = array(
				'nome' => $parametros['nome'],
				'estado' => $parametros['select'],
				'data' => $parametros['data']);

			$this->clientemodel->alterar($id,$dados);

			redirect('clienteController/buscar','refresh');
		}

		
		$resultadoBusca = $this->clientemodel->id($id);
		$dados['resultado_busca'] = $resultadoBusca;
		$this->load->view('editar',$dados);
	}

	
}