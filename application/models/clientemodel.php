<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clientemodel extends CI_Model{
	public function inserir($dados = NULL){
		if($dados != NULL){
			$this->db->insert('cadastro',$dados);
		}
	}

	public function busca(){
		$this->db->select('*');
		$this->db->from('cadastro');
		$query = $this->db->get();
		return $query->result();
	}

	public function id($id = NULL){
		if($id != NULL){
			$this->db->select('*');
			$this->db->from('cadastro');
			$this->db->where('id_cadastro',$id);
			$query = $this->db->get();
			return $query->result();
		}
	}

	public function alterar($id = NULL,$dados = NULL){
		if($dados != NULL){
			$this->db->where('id_cadastro',$id);
			$this->db->update('cadastro',$dados);
		}
	}
}